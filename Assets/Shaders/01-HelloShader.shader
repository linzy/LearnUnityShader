﻿Shader "LearnShader/01 HelloShader"{
	// 属性
	Properties{
		// 使用斜杠命名防止与关键字冲突
		_Color("Color",Color) = (1,1,1,1)    // float4
		_Vector("Vector",Vector) = (2,3,4,5)   // float4
		_Int("Int",Int) = 123            // float
		_Float("Float",Float) = 4.5      // float
		_Range("Range",Range(2,6))=3     // float
		_2D("Texture",2D) = "white"{}    // sampler2D
		_Cube("Cube",Cube) = "white"{}   // samplerCube
		_3D("Texture",3D) = "black"{}    // sampler3D
	}

		// 整个语言的结构称为shaderLab
		// 多个SubShader可以按效果降序
		SubShader{
		// 至少有一个Pass
		// 一个pass相当于一个方法
		Pass{
		// HLSLPROGRAM or CGPROGRAM
		CGPROGRAM
		// 使用CG语言编写shader代码

		// 重新定义需要使用的值并取得Properties中的值
		float4 _Color;   // float 234 half fixed
	    float4 _Vector;
		float _Int;
		float _Float;
		float _Range;
		sampler2D _2D;
		samplerCube _Cube;
		sampler3D _3D;

			ENDCG
        }
	}

	// 后备方案
	FallBack "VertexLit"
}