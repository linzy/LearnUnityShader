﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "LearnShader/02-DefaultCopy" {
	SubShader{
		Pass{
		CGPROGRAM
		// 顶点函数
#pragma vertex vert  // 顶点函数函数名
		// 片元函数
#pragma fragment frag  // 片元函数的函数名

		float4 vert(float4 v : POSITION) : SV_POSITION {
			return UnityObjectToClipPos(v);
        }

		float4 frag() : SV_Target{
			return fixed4(0.5,1,1,1);
        }

		ENDCG
          }
	}
}
