﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "LearnShader/03-UseStruct" {
	SubShader{
		Pass{
		CGPROGRAM
		// 顶点函数
#pragma vertex vert  // 顶点函数函数名
		// 片元函数
#pragma fragment frag  // 片元函数的函数名

		struct a2v {
			float4 vertex:POSITION;  // 获得模型空间下的顶点坐标
			float3 normal:NORMAL;   // 获得法线方向
			float4 texcoord:TEXCOORD0;  // 获得第一套纹理坐标
        };

	    struct v2f {
		    float4 position:SV_POSITION;
			float3 temp:COLOR0;
		};

		v2f vert(a2v v){
			v2f f;
			f.position = UnityObjectToClipPos(v.vertex);
			f.temp = v.normal;
			return f;
	    }

		float4 frag(v2f f) : SV_Target{
		return fixed4(f.temp,1);
	    }

		ENDCG
	   }
	}
}
