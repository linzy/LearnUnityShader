﻿// Upgrade NOTE: replaced '_World2Object' with 'unity_WorldToObject'

// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "LearnShader/04-DiffuseVertex" {

	Properties{
		_Diffuse("Diffuse Color",Color) = (1,1,1,1)
	}

		SubShader{
			Pass{
			Tags{"LightMode" = "ForwardBase"}   // 标签得到unity内置光照变量
			CGPROGRAM

	#include "Lighting.cginc"    // 包含unity内置文件

		// 顶点函数
#pragma vertex vert  // 顶点函数函数名
		// 片元函数
#pragma fragment frag  // 片元函数的函数名

		fixed4 _Diffuse;

		struct a2v {
			float4 vertex:POSITION;  // 获得模型空间下的顶点坐标

			float3 normal:NORMAL;
        };

	    struct v2f {
		    float4 position:SV_POSITION;
			fixed3 color : COLOR;
		};

		v2f vert(a2v v){
			v2f f;
			f.position = UnityObjectToClipPos(v.vertex);

			fixed3 ambient = UNITY_LIGHTMODEL_AMBIENT.rgb; // 获得环境光

			fixed3 normalDir = normalize(mul(v.normal,(float3x3)unity_WorldToObject));

			fixed3 lightDir = normalize(_WorldSpaceLightPos0.xyz);  // 光对于每个顶点的方向

			fixed3 diffuse = _LightColor0.rgb * max(0, dot(normalDir, lightDir)) * _Diffuse.rgb; // 获得漫反射颜色

			f.color = diffuse + ambient;

			return f;
	    }

		float4 frag(v2f f) : SV_Target{
		return fixed4(f.color,1);
	    }

		ENDCG
	   }
	}
}
