﻿// Upgrade NOTE: replaced '_World2Object' with 'unity_WorldToObject'

// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "LearnShader/06-DiffuseFragment HalfLambert" {

	Properties{
		_Diffuse("Diffuse Color",Color) = (1,1,1,1)
	}

		SubShader{
			Pass{
			Tags{"LightMode" = "ForwardBase"}   // 标签得到unity内置光照变量
			CGPROGRAM

	#include "Lighting.cginc"    // 包含unity内置文件

		// 顶点函数
#pragma vertex vert  // 顶点函数函数名
		// 片元函数
#pragma fragment frag  // 片元函数的函数名

		fixed4 _Diffuse;

		struct a2v {
			float4 vertex:POSITION;  // 获得模型空间下的顶点坐标

			float3 normal:NORMAL;
        };

	    struct v2f {
		    float4 position:SV_POSITION;
			fixed3 worldNormalDir : COLOR0;
		};

		v2f vert(a2v v){
			v2f f;
			f.position = UnityObjectToClipPos(v.vertex);

			f.worldNormalDir = mul(v.normal, (float3x3)unity_WorldToObject);

			return f;
	    }

		float4 frag(v2f f) : SV_Target{

			fixed3 ambient = UNITY_LIGHTMODEL_AMBIENT.rgb; // 获得环境光

		fixed3 normalDir = normalize(f.worldNormalDir);

		fixed3 lightDir = normalize(_WorldSpaceLightPos0.xyz);  // 光对于每个顶点的方向
		float halfLambert = dot(normalDir, lightDir) * 0.5 + 0.5;
		fixed3 diffuse = _LightColor0.rgb * halfLambert * _Diffuse.rgb; // 获得漫反射颜色

		fixed3 tempColor = diffuse + ambient;

		return fixed4(tempColor,1);
	    }

		ENDCG
	   }
	}
}
