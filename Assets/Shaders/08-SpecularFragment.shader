﻿// Upgrade NOTE: replaced '_World2Object' with 'unity_WorldToObject'

// Upgrade NOTE: replaced '_World2Object' with 'unity_WorldToObject'

// Upgrade NOTE: replaced '_World2Object' with 'unity_WorldToObject'

// Upgrade NOTE: replaced '_World2Object' with 'unity_WorldToObject'

// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "LearnShader/08-SpecularFragment" {

	Properties{
		_Diffuse("Diffuse Color",Color) = (1,1,1,1)
		_Specular("Specular Color",Color) = (1,1,1,1)
		_Gloss("Gloss",Range(8,200)) = 10
	}

		SubShader{
			Pass{
			Tags{"LightMode" = "ForwardBase"}   // 标签得到unity内置光照变量
			CGPROGRAM

	#include "Lighting.cginc"    // 包含unity内置文件

		// 顶点函数
#pragma vertex vert  // 顶点函数函数名
		// 片元函数
#pragma fragment frag  // 片元函数的函数名
		fixed4 _Diffuse;
	fixed4 _Specular;
	    half _Gloss;

		struct a2v {
			float4 vertex:POSITION;  // 获得模型空间下的顶点坐标

			float3 normal:NORMAL;
        };

	    struct v2f {
		    float4 position:SV_POSITION;
			float3 worldNormal : TEXCOORD0;
			float3 worldVertex:TEXCOORD1;
		};

		v2f vert(a2v v){

			v2f f;
			f.position = UnityObjectToClipPos(v.vertex);
			f.worldNormal = mul(v.normal, (float3x3)unity_WorldToObject);
			f.worldVertex = mul(v.vertex, unity_WorldToObject).xyz;
			
			return f;
	    }

		float4 frag(v2f f) : SV_Target{

			fixed3 ambient = UNITY_LIGHTMODEL_AMBIENT.rgb; // 获得环境光

		fixed3 normalDir = normalize(f.worldNormal);

		fixed3 lightDir = normalize(_WorldSpaceLightPos0.xyz);  // 光对于每个顶点的方向

		fixed3 diffuse = _LightColor0.rgb * max(0, dot(normalDir, lightDir)) * _Diffuse.rgb; // 获得漫反射颜色

																							 // 高光反射   
		fixed3 reflectDir = normalize(reflect(-lightDir, normalDir));        // 根据法线获得反射光

		fixed3 viewDir = normalize(_WorldSpaceCameraPos.xyz - f.worldVertex);  // 获得视野方向(摄像机-顶点)
		fixed3 specular = _LightColor0.rgb *_Specular.rgb * pow(max(dot(reflectDir, viewDir), 0), _Gloss);


		fixed3 tempColor = diffuse + ambient + specular;


		return fixed4(tempColor,1);
	    }

		ENDCG
	   }
	}
}
