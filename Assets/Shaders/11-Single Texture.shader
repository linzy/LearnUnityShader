﻿// Upgrade NOTE: replaced '_World2Object' with 'unity_WorldToObject'
// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "LearnShader/11-Texture"{  // shader定义
	Properties{         // 属性
		//_Diffuse("Diffuse Color",Color) = (1,1,1,1)   // 漫反射颜色
		_Color("Color",Color)=(1,1,1,1)
		_MainTex("MainTex",2D) = "white"{}    // 纹理
		_Specular("Specular Color",Color) = (1,1,1,1)  // 高光反射颜色
		_Gloss("Gloss",Range(10,200)) = 20        // 高光系数
	}
		SubShader{
			Pass{
			Tags{"LightMode" = "ForwardBase"}
				CGPROGRAM

	#include "Lighting.cginc"       // unity内置cg文件
	#pragma vertex vert
	#pragma fragment frag

			//fixed4 _Diffuse;
			  fixed4 _Color;
			  sampler2D _MainTex; 
			  float4 _MainTex_ST;   // 材质缩放和偏移
              fixed4 _Specular;
			  half _Gloss;

			  struct a2v {
				  float4 vertex:POSITION;   // 顶点位置
				  float3 normal:NORMAL;        // 法线
				  float4 texcoord:TEXCOORD0;
			  };
			  struct v2f {
				  float4 svPos:SV_POSITION;       // 屏幕坐标下的顶点位置
				  float3 worldNormal:TEXCOORD0;    // 世界空间下的法线
				  float4 worldVertex:TEXCOORD1;     // 世界空间下的顶点坐标
				  float2 uv:TEXCOORD2; 
			  };

			  v2f vert(a2v v) {
				  v2f f;
				  f.svPos = UnityObjectToClipPos(v.vertex);    // 模型坐标转屏幕坐标
				  f.worldNormal = UnityObjectToWorldNormal(v.normal);
				  f.worldVertex = mul(v.vertex, unity_WorldToObject);
				  f.uv = v.texcoord.xy + _MainTex_ST.zw;   // *缩放 +-偏移
				  return f;
			  }

			  fixed4 frag(v2f f) :SV_Target{

				  fixed3 normalDir = normalize(f.worldNormal);   // 获得法线向量
			  fixed3 lightDir = normalize(WorldSpaceLightDir(f.worldVertex));   // 取得光的方向

			  // 取得纹理上像素点的颜色
			  fixed3 texColor = tex2D(_MainTex, f.uv.xy) * _Color.rgb;

			  fixed3 diffuse = _LightColor0.rgb * texColor * max(dot(normalDir, lightDir), 0); // 漫反射颜色

			  fixed viewDir = normalize(UnityWorldSpaceViewDir(f.worldVertex));   // 摄像机方向
			  fixed3 halfDir = normalize(lightDir + viewDir);       // 光方向和摄像机方向平分线
			  fixed3 specular = _LightColor0.rgb * _Specular.rgb * pow(max(dot(normalDir, halfDir), 0), _Gloss);

			  // 和环境光做融合
			  fixed3 tempColor = diffuse + specular + UNITY_LIGHTMODEL_AMBIENT.rgb*texColor;
			  return fixed4(tempColor, 1);
			  }

		    ENDCG
        }
	}
}