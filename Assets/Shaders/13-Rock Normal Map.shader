﻿// Upgrade NOTE: replaced '_World2Object' with 'unity_WorldToObject'
// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "LearnShader/13-Rock Normal Map"{  // shader定义
	Properties{// 属性
		//_Diffuse("Diffuse Color",Color) = (1,1,1,1)   // 漫反射颜色
		_Color("Color",Color) = (1,1,1,1)
		_MainTex("Main Tex",2D) = "white"{}
	_NormalMap("Normal Map",2D) = "bump"{}
	_BumpScale("Bump Scale",Float) = 1
	}
		SubShader{
		Pass{
		Tags{ "LightMode" = "ForwardBase" }
		CGPROGRAM

#include "Lighting.cginc"
#pragma vertex vert
#pragma fragment frag

//fixed4 _Diffuse;
fixed4 _Color;
	sampler2D _MainTex;
	float4 _MainTex_ST;
	sampler2D _NormalMap;
	float4 _NormalMap_ST;
	float _BumpScale;
	
	struct a2v {
		float4 vertex:POSITION;   // 顶点位置
        // 切线空间通过模型里的法线和切线确定
		float3 normal:NORMAL;        // 法线
		float4 tangent:TANGENT;  // tangent.w 确定切线空间中坐标轴的方向
		float4 texcoord:TEXCOORD0;
	};
	struct v2f {
		float4 svPos:SV_POSITION;       // 屏幕坐标下的顶点位置
		float3 lightDir:TEXCOORD0;       // 切线空间下 平行光的方向
		//float4 worldVertex:TEXCOORD1;     // 世界空间下的顶点坐标
		float4 uv:TEXCOORD1;    // x y存 MainTex纹理坐标 zw存NormalMap纹理坐标
	};

	v2f vert(a2v v) {
		v2f f;
		f.svPos = UnityObjectToClipPos(v.vertex);    // 模型坐标转屏幕坐标
		//f.worldVertex = mul(v.vertex, unity_WorldToObject);
		f.uv.xy = v.texcoord.xy * _MainTex_ST.xy + _MainTex_ST.zw;   // *缩放 +-偏移
		f.uv.zw = v.texcoord.xy * _NormalMap_ST.xy + _NormalMap_ST.zw;

		// 调用宏得到矩阵 rotation 把模型空间下的方向做换成切线空间下
		TANGENT_SPACE_ROTATION;
		f.lightDir = mul(rotation,ObjSpaceLightDir(v.vertex));
		// f.lightDir = UnityObjectToClipPos(ObjSpaceLightDir(v.vertex));

		return f;
	}

	// 把发现方向有关的运算放在切线空间下
	// 法线贴图里取得的法线方向是在切线空间下的
	fixed4 frag(v2f f) :SV_Target{

		//fixed3 normalDir = normalize(f.worldNormal);   // 获得法线向量(模型空间)
   fixed4 normalColor = tex2D(_NormalMap,f.uv.zw); 
   //fixed3 tangentNormal = normalize(normalColor.xyz * 2 - 1); // 切线空间下的法线
	fixed3 tangentNormal = UnpackNormal(normalColor);
	tangentNormal.xy = tangentNormal.xy * _BumpScale;
	tangentNormal = normalize(tangentNormal);

												   //fixed3 lightDir = normalize(WorldSpaceLightDir(f.worldVertex));   // 取得光的方向
   fixed3 lightDir = normalize(f.lightDir);   // 取得光的方向

											  // 取得纹理上像素点的颜色
   fixed3 texColor = tex2D(_MainTex, f.uv.xy) * _Color.rgb;

   fixed3 diffuse = _LightColor0.rgb * texColor * max(dot(tangentNormal, lightDir), 0); // 漫反射颜色


																					// 和环境光做融合
   fixed3 tempColor = diffuse + UNITY_LIGHTMODEL_AMBIENT.rgb*texColor;
   return fixed4(tempColor, 1);
   }

	   ENDCG
   }
	}
		//Fallback "Specular"
}